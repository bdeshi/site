<?php
  $title = "envs.net | chat";
  $desc = "envs.net | chat";

include '../header.php';
?>

  <body id="body" class="dark-mode">
    <div>

      <div class="button_back">
        <pre class="clean"><strong><a href="/">&lt; back</a></strong></pre>
      </div>

      <div id="main">
<div class="block">
<pre>
<h1><em>chat</em></h1>

envs.net has two chat options. we can be reached via irc and matrix.

<strong>rules / guidelines</strong>
<em>please see the <a href="https://tilde.chat/wiki/?page=etiquette" target="_blank">etiquette guide</a>.</em>
</pre>
</div>

<pre>

<h3>&#35; more informations about</h3>
&nbsp;&nbsp;<strong><a href="/chat/matrix">&gt; matrix chat</a></strong>
&nbsp;&nbsp;<strong><a href="/chat/irc">&gt; irc chat</a></strong>
</pre>

      </div>

<?php include '../footer.php'; ?>
