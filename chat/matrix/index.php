<?php
  $title = "envs.net | matrix chat";
  $desc = "envs.net | matrix chat";

include '../../header.php';
?>

  <body id="body" class="dark-mode">
    <div class="clear" style="min-width: 1280px;">

      <div class="button_back">
        <pre class="clean"><strong><a href="/chat">&lt; back</a></strong></pre>
      </div>

      <div id="main">
<div class="block">
<pre>
<h1><em>matrix chat</em></h1>

our self-hosted matrix instance and web-client.

<a href="https://matrix.envs.net/" target="_blank">matrix.envs.net</a>
</pre>
</div>

<pre>

An open network for secure, decentralized communication.

<a href="https://matrix.envs.net/_matrix/client/#/register" target="_blank"><strong>&gt; create your matrix account on envs. &lt;</strong></a>


<h3>&#35; matrix client software</h3>
<a href="https://about.riot.im/downloads" target="_blank">check out the official riot client!</a>


<h3>&#35; links</h3>
<a href="https://matrix.org/" target="_blank">matrix.org</a>
<a href="https://riot.im/" target="_blank">riot.im</a>

</pre>

      </div>

      <div id="sidebar">

<div class="block">
<pre>
group
<code>+envs:matrix.envs.net</code>

channel
<code>#envs:matrix.envs.net</code>
</pre>
</div>

<div class="block">
<pre>
<table>
  <tr onclick="window.location='https://matrix.envs.net/';">
    <td width="25px"><i class="fa fa-commenting-o fa-fw" aria-hidden="true"></i></td> <td><a href="https://matrix.envs.net/">web-client</a></td>
  </tr>
</table></pre>
</div>

<div class="block">
<pre>
<h3>rules / guidelines</h3>
<em>please see the <a href="https://tilde.chat/wiki/?page=etiquette" target="_blank">etiquette guide</a>.</em>
</pre>
</div>

      </div>

<?php include '../../footer.php'; ?>
