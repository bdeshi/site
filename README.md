envs.net is a minimalist, non-commercial
shared unix system and will always be free to use.<br>
<br>
we are linux lovers, sysadmins, programmer and users who like build
webpages, write blogs, chat online, play cool console games and so much
more. you wish to join with an small user space?<br>

join the team today!<br>
<a href="https://envs.net/signup">signup for a envs.net account</a><br>
<br>
inspired by [&#126;team](https://tilde.team/) (tilde.team) and a member of [tildeverse](https://tildeverse.org/).
