<?php
  $title = "envs.net | code of conduct";
  $desc = "envs.net | code of conduct (coc)";

include 'header.php';
?>

  <body id="body" class="dark-mode">
    <div>

      <div class="button_back">
        <pre class="clean"><strong><a href="/">&lt; back</a></strong></pre>
      </div>

      <div id="main">
<div class="block_head">
<pre>
<h1><em>code of conduct</em></h1>

<strong>tl;dr</strong> learn by doing and helping. be excellent to each other. have fun!
</pre>
</div>

<pre>
<h3 id="purpose">1. purpose</h3>
a primary goal of envs.net is to be inclusive to the largest number of
contributors, with the most varied and diverse backgrounds possible. as such, we
are committed to providing a friendly, safe and welcoming environment for all.
this code of conduct outlines our expectations for all those who participate in
our community, as well as the consequences for unacceptable behavior.
we invite all those who participate in envs.net to help us create safe and
positive experiences for everyone.

<h3 id="envs-mission">2. envs.net mission</h3>
<strong>envs.net exists to foster an engaged community for socializing, learning, and making cool stuff</strong>
in a mass-media age, it is up to small, intentional communities to gather and
work together to provide a space outside of the advertising-laden,
profit-seeking, corporate-owned world of social media.
to that end, envs.net strives to be a radically inclusive community where people
of all backgrounds and all technological experience levels can come together to
learn, to teach, and to delight in one another’s creations.

<h3 id="expected-behavior">3. expected behavior</h3>
the following behaviors are expected and requested of all community members:
<ul>
<li>participate in an authentic and active way. in doing so, you contribute to
the health and longevity of this community. * exercise consideration and respect
in your speech and actions. * attempt collaboration before conflict.</li>
<li>refrain from demeaning, discriminatory, or harassing behavior and speech.</li>
<li>be mindful of your surroundings and of your fellow participants. * teach
when people need help. don’t do it for them.</li>
</ul>
<h3 id="unacceptable-behavior">4. unacceptable behavior</h3>
the following behaviors are considered harassment and are unacceptable within our community:
<ul>
<li>violence, threats of violence or violent language directed against another person.</li>
<li>sexist, racist, homophobic, transphobic, ableist or otherwise discriminatory jokes and language.</li>
<li>posting or displaying sexually explicit or violent material.</li>
<li>posting or threatening to post other people’s personally identifying information (“doxing”).</li>
<li>personal insults, particularly those related to gender, sexual orientation, race, religion, or disability.</li>
<li>unwelcome sexual attention. this includes sexualized comments or jokes.</li>
<li>deliberate intimidation, stalking or following.</li>
</ul>
<h3 id="consequences-of-unacceptable-behavior">5. consequences of unacceptable behavior</h3>
unacceptable behavior from any community member, including sponsors and those
with decision-making authority (sudo), will not be tolerated.
anyone asked to stop unacceptable behavior is expected to comply immediately.
if a community member engages in unacceptable behavior, the community organizers
may take any action they deem appropriate, up to and including a temporary ban
or permanent expulsion from envs.net without warning (meaning your account will
be terminated and all user data deleted).

<h3 id="reporting-guidelines">6. reporting guidelines</h3>
if you are subject to or witness unacceptable behavior, or have any other
concerns, please contact an admin (see info below).
additionally, help engaging with law enforcement is available.

<h3 id="addressing-grievances">7. addressing grievances</h3>
if you feel you have been falsely or unfairly accused of violating this code of conduct,
use the contact info below to send a concise description of your grievance.

<h3 id="scope">8. scope</h3>
we expect all envs.net members to abide by this code of conduct while:
<ul>
<li>engaging with other members</li>
<li>publishing content on envs.net</li>
</ul>
<h3 id="contact-info">9. contact info</h3>
envs.net admins:
 &gt; you can also send a mail to <a href="mailto:sudoers@envs.net">sudoers@envs.net</a> to make sure we all get it.
<ul>
<li><a href="https://envs.net/~creme/">~creme</a>:</li>
<ul class="sublist">
<li><a href="mailto:creme@envs.net">creme@envs.net</a></li>
<li>on irc <code>/query creme hello</code></li>
</ul>
</ul>
<h3 id="license-and-attribution">10. license and attribution</h3>
this code of conduct is based on <a href="http://citizencodeofconduct.org/" target="_blank">citizencodeofconduct.org</a> and <a href="http://tilde.town/wiki/conduct.html" target="_blank">tilde.town coc</a>
under the terms of the <a href="http://creativecommons.org/licenses/by-sa/3.0/" target="_blank">creative commons attribution-sharealike license</a>.

<h3 id="tos">11. terms of service</h3>
please also see the <a href="/tos/">terms of service</a>.

</pre>
      </div>

<?php include 'footer.php'; ?>
