<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?=$title ?? "envs.net | environments"?></title>
    <meta name="description" content="<?=$desc ?? "envs.net | environments for linux lovers - since 9/2019"?>" />
    <meta name="author" content="Sven Kinne" />
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="7 days">
    <meta name="keywords" content="Sven Kinne,env,envs,enviroments,webpage,blog,gopher,forum,bbj,searx,news,feed,rss,atom,collaborative,pad,cryptpad,code,codepad,pastebin,pb,termbin,tb,file,curl,0x0,shorten,shorter,shortener,share,upload,ip,ifconfig,ipconfig,showip,whois,userspace,space,real-time,tiny,minimalist,shared,non-commercial,unix,linux,debian,bash,programmer,hackers,console,hosting" />
    <link rel="stylesheet" href="/css/css_style.css" />
    <link rel="stylesheet" href="/css/fork-awesome.min.css" />
    <?=$additional_head ?? ""?>
    <?php unset($title); unset($desc); unset($additional_head); ?>

  </head>
