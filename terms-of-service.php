<?php
  $title = "envs.net | terms of service";
  $desc = "envs.net | terms of service (tos)";

include 'header.php';
?>

  <body id="body" class="dark-mode">
    <div>

      <div class="button_back">
        <pre class="clean"><strong><a href="/">&lt; back</a></strong></pre>
      </div>

      <div id="main">
<div class="block">
<pre>
<h1><em>terms of service</em></h1>
</pre>
</div>

<pre>
the following offences will result in a service ban.
here are some things that we don't allow:
<ul>
<li>anything illegal or harmful</li>
<li>promoting racial, ethnic, religious, political & other forms of bigotry</li>
<li>deliberately defacing the accounts of other users</li>
<li>deliberately trying to disrupt envs.net's server</li>
<li>using envs.net for disrupting other servers</li>
<li>using envs.net to impersonate other websites and businesses for criminal purposes</li>
<li>distributing pornography of any genre and medium (especially child pornography)</li>
<li>distributing content that defames any individual</li>
<li>outgoing net- and portscanning is prohibited on envs.net</li>
<li>hosting your backups</li>
<li>mining cryptocurrencies</li>
</ul>
</pre>

<div class="block">
<pre>
<h1><em>resource usage</em></h1>
</pre>
</div>

<pre>
please make sure to not run anything that would disrupt other users.
there are a few limits set on your accounts:
<ul>
<li>200 process/threads at a time</li>
<li>soft 1024mb/hard 1536mb limit on storage</li>
<li>250mb limit on mailbox storage</li>
</ul>
</pre>
      </div>

<?php include 'footer.php'; ?>
